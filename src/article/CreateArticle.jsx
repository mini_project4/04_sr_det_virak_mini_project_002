import React, {  useState } from "react";

import { Button, Container, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { api } from "../sevices/API";


export default function CreateArticle() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [published, setPublished] = useState(true);
  const [image, setImage] = useState('');
  const [imgURL,setImgURL] = useState()
  const navigate = useNavigate()
  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };
  const handleDesChange = (e) => {
    setDescription(e.target.value);
    console.log(e.target.value);
  };
  const handlePublishedChange = (e) => {
    console.log(e.target.checked);
    setPublished(e.target.checked);
  };
  const handleImageChange = (e) => {
    console.log('the link',e.target.files[0]);
    console.log('URL',URL.createObjectURL(e.target.files[0]));
    setImgURL(URL.createObjectURL(e.target.files[0]));
    const formData = new FormData();
    formData.append('image', e.target.files[0]);
    console.log('form data:', formData.get('image'));
    api.post('images',formData)
    .then((res) => setImage(res.data.payload.url));
  };
  const handleAdd = () => {
      api
        .post("articles", { title, description, published,image })
        .then((res) => console.log(res.data.message));
        navigate('/')
  }
  return (
    <div>
      <Container>
        <h1>Create New Article</h1>
        <Form className="w-50">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Title"
              onChange={handleTitleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Description"
              onChange={handleDesChange}
            />
          </Form.Group>
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Image</Form.Label>
            <Form.Control type="file" onChange={handleImageChange}/>
          </Form.Group>
          <img className="border border-warning" src={imgURL ?? 'https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg'} alt='preview' style={{height:'200px'}}/>
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox"
              label="Is Published"
              onChange={handlePublishedChange}
            />
          </Form.Group>
          <Button style={{width:'75px',marginRight:'5px'}} variant="danger" onClick={() => navigate('/')}>Cancel</Button>
          <Button style={{width:'75px'}} variant="info" onClick={handleAdd}>
            Add
          </Button>
          
        </Form>
      </Container>
    </div>
  );
}
