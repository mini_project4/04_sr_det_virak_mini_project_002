
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap';
import { Link, Outlet } from 'react-router-dom';
import Articles from '../components/Articles';
import { api } from '../sevices/API';

export default function AllArticle() {
    const [data,setData] = useState([])
    useEffect(() => {
        api.get('articles')
        .then(res => {setData(res.data.payload)});
    })
    const deleteArticle = (id) => {
        const afterDelete = data.filter(items => items._id != id)
        setData(afterDelete)
        console.log(id);
        api.delete('articles/' + id)
        .then(res => console.log(res.data.message))
      }
  return (
    <div>
        <Container>
            <div style={{marginTop:'10px'}}>
                <h1 style={{display:'inline'}}>All Articles</h1>
                <Button variant='success' as={Link} to='addArticle'style={{float:'right'}}>Add Article</Button>
            </div>
            
            <Row>
                {
                    data.map((obj,index) => (
                        <Col key={index} md={2}>
                        <Articles obj ={obj} deleteArticle={deleteArticle}/>
                        </Col>
                    ))
                }
                
            </Row>
            
        </Container>
        <Outlet/>
    </div>
  )
}
