import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { api } from "../sevices/API";

export default function CreateCategory() {
  const [name, setName] = useState("");
  const navigate = useNavigate();
  const handleNameChange = (e) => {
    setName(e.target.value);
  };
  const handleAdd = () => {
    api
      .post("/category", { name })
      .then((res) => console.log(res.data.message));
    navigate("/category");
  };

  return (
    <div className='container'>
      <br/>
      <h1>Add New Category</h1>
      <Form className="w-50">
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Category Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter name"
            onChange={handleNameChange}
          />
        </Form.Group>
        <Button variant="warning" onClick={handleAdd}>
          Add
        </Button>
      </Form>
    </div>
  );
}
