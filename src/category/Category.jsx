import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Categories from "../components/Categories";
import { api } from "../sevices/API";

export default function Category() {
  const [data, setData] = useState([]);
  useEffect(() => {
    api.get("category").then((res) => {
      setData(res.data.payload);
    });
  }, []);
  const deleteCategory = (id) => {
    const afterDelete = data.filter((items) => items._id != id);
    setData(afterDelete);
    console.log(id);
    api.delete("category/" + id).then((res) => console.log(res.data.message));
  };

  return (
    <div>
      <Container>
        <div style={{marginTop:'10px'}}>
                <h1 style={{display:'inline'}}>All Articles</h1>
                <Button variant='success' as={Link} to='/addNewCategory'style={{float:'right'}}>Add Category</Button>
            </div>
        <Row>
          {data.map((obj, index) => (
            <Col key={index}>
              <Categories obj={obj} deleteCategory={deleteCategory} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
