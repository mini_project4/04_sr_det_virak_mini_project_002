import React from 'react'
import { Button } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';

export default function ViewCategory() {
    const loc = useLocation();
    const data = loc.state;
    const nav = useNavigate();
  return (
    <div className='container' >
        <br/>
        <h1>View Category</h1>
        <br/>
        <Button variant='warning' onClick={() => nav('/category')}>Back</Button>
        
        <h1>Category name: {data.name}</h1>
    </div>
  )
}
