
import { Route, Routes } from 'react-router-dom';
import './App.css';
import AllArticle from './article/AllArticle';
import CreateArticle from './article/CreateArticle';
import EditArticle from './article/EditArticle';
import ViewArticle from './article/ViewArticle';
import Category from './category/Category';
import CreateCategory from './category/CreateCategory';
import EditCategory from './category/EditCategory';
import ViewCategory from './category/ViewCategory';
import NavBar from './components/NavBar';

function App() {
  return (
    <div className="App" style={{fontFamily:'Comic Sans MS'}}>
     <NavBar/>
     <Routes>
       <Route path='/' element = {<AllArticle/>}/>
         <Route path='addArticle' element={<CreateArticle/>}/>
       <Route path='/category' element = {<Category/>}/>
       <Route path='addNewCategory' element={<CreateCategory/>}/>
       <Route path='viewCategory' element={<ViewCategory/>}/>
       <Route path='editCategory' element={<EditCategory/>}/>
       <Route path='viewArticle' element={<ViewArticle/>}/>
       <Route path='editArticle' element={<EditArticle/>}/>
     </Routes>
    </div>
  );
}

export default App;
