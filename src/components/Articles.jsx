import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";


export default function Articles({obj,deleteArticle}) {

  const nav = useNavigate()
  const viewArticle = () => {
    nav('/viewArticle',{state: {...obj}})
}
const onUpdate = () => {
  nav("/editArticle", { state: { ...obj } });
};

  return (
    <div>
      <Card style={{ width: "12rem",borderRadius:'15px',marginTop:'25px' }}>
        <Card.Img variant="top" src={obj.image ?? 'https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg'} style={{height:'20vh'}} />
        <Card.Body>
          <Card.Title className="title"  style={{height:'5vh',textAlign:'center',textTransform:"capitalize"}}>{obj.title}</Card.Title>
          <Card.Text className="des" style={{height:'8vh',textAlign:'center'}}>
            {obj.description}
          </Card.Text>
          <Button variant="primary" className="w-100 mb-2" onClick={() => onUpdate(obj._id)}>Edit</Button>
          <Button variant="danger" className="w-100 mb-2" onClick={viewArticle}>View</Button>
          <Button variant="warning" className="w-100" onClick={() => deleteArticle(obj._id)}>Delete</Button>
        </Card.Body>
      </Card>
    </div>
  );
}
