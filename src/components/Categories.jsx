
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function Categories({obj,deleteCategory}) {
  const nav = useNavigate()
  const viewCategory = () => {
    nav('/viewCategory',{state: {...obj}})
}
const onUpdate = (name, id) => {
  nav("/editCategory", { state: { name, id} });
};

  
  return (
    <div>
      <Card style={{ width: "12rem",borderRadius:'15px',marginTop:'25px',backgroundColor: 'lightblue' }}>
        <Card.Body>
          <Card.Title style={{textTransform:'capitalize',textAlign:'center'}}>{obj.name}</Card.Title>
          <Button variant="success" className="w-100 mb-2" onClick={() => onUpdate(obj.name, obj._id)}>Edit</Button>
          <br/>
          <Button variant="info" className="w-100 mb-2" onClick={viewCategory}>View</Button>
          <br/>
          <Button variant="danger" className="w-100" onClick={() => deleteCategory(obj._id)}>Delete</Button>
        </Card.Body>
      </Card>
      
    </div>
  );
}
